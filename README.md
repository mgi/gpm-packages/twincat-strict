# TwinCAT Strict

The TwinCAT LabVIEW library uses variants to deal with typing. This library adds wrappers to provide a strictly typed interface. Because it's based on the TwinCAT LabVIEW library, the two timing options are synchronous and asynchronous polling. Asynchronous polling code is currently being tested.

If you want to create an event driven interface instead of a polled interface, you need to switch to using the .NET calls instead of the LabVIEW library. We have some internal code for using .NET but it doesn't implement event based messaging yet, just synchronous messaging, but it could be a starting point for doing event based.
