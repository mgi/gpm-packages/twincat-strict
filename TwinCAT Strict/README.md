# TwinCAT Strict

The TwinCAT LabVIEW library uses variants to deal with typing. This library adds wrappers to provide a strictly typed interface.

Basic TwinCAT types are included and also some arrays of those types. Other types are straightforward to add.

Only synchronous reading and writing is currently implemented but asynchronous operations could be added.