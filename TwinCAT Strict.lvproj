﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Reader" Type="Folder">
			<Item Name="Synchronous" Type="Folder">
				<Item Name="TCR 1D Boolean" Type="Folder">
					<Item Name="TCR 1D Boolean.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR 1D Boolean/TCR 1D Boolean.lvclass"/>
				</Item>
				<Item Name="TCR 1D Double" Type="Folder">
					<Item Name="TCR 1D Double.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR 1D Double/TCR 1D Double.lvclass"/>
				</Item>
				<Item Name="TCR 2D Boolean" Type="Folder">
					<Item Name="TCR 2D Boolean.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR 2D Boolean/TCR 2D Boolean.lvclass"/>
				</Item>
				<Item Name="TCR 2D Double" Type="Folder">
					<Item Name="TCR 2D Double.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR 2D Double/TCR 2D Double.lvclass"/>
				</Item>
				<Item Name="TCR Boolean" Type="Folder">
					<Item Name="TCR Boolean.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR Boolean/TCR Boolean.lvclass"/>
				</Item>
				<Item Name="TCR Double" Type="Folder">
					<Item Name="TCR Double.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR Double/TCR Double.lvclass"/>
				</Item>
				<Item Name="TCR I8" Type="Folder">
					<Item Name="TCR I8.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR I8/TCR I8.lvclass"/>
				</Item>
				<Item Name="TCR I16" Type="Folder">
					<Item Name="TCR I16.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR I16/TCR I16.lvclass"/>
				</Item>
				<Item Name="TCR I32" Type="Folder">
					<Item Name="TCR I32.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR I32/TCR I32.lvclass"/>
				</Item>
				<Item Name="TCR I64" Type="Folder">
					<Item Name="TCR I64.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR I64/TCR I64.lvclass"/>
				</Item>
				<Item Name="TCR Single" Type="Folder">
					<Item Name="TCR Single.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR Single/TCR Single.lvclass"/>
				</Item>
				<Item Name="TCR String" Type="Folder">
					<Item Name="TCR String.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR String/TCR String.lvclass"/>
				</Item>
				<Item Name="TCR U8" Type="Folder">
					<Item Name="TCR U8.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR U8/TCR U8.lvclass"/>
				</Item>
				<Item Name="TCR U16" Type="Folder">
					<Item Name="TCR U16.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR U16/TCR U16.lvclass"/>
				</Item>
				<Item Name="TCR U32" Type="Folder">
					<Item Name="TCR U32.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR U32/TCR U32.lvclass"/>
				</Item>
				<Item Name="TCR U64" Type="Folder">
					<Item Name="TCR U64.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/Synchronous/TCR U64/TCR U64.lvclass"/>
				</Item>
			</Item>
			<Item Name="TwinCAT Reader.lvclass" Type="LVClass" URL="../TwinCAT Strict/Reader/TwinCAT Reader.lvclass"/>
		</Item>
		<Item Name="Writer" Type="Folder">
			<Item Name="Synchronous" Type="Folder">
				<Item Name="TCW 1D Boolean" Type="Folder">
					<Item Name="TCW 1D Boolean.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW 1D Boolean/TCW 1D Boolean.lvclass"/>
				</Item>
				<Item Name="TCW 1D Double" Type="Folder">
					<Item Name="TCW 1D Double.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW 1D Double/TCW 1D Double.lvclass"/>
				</Item>
				<Item Name="TCW 2D Double" Type="Folder">
					<Item Name="TCW 2D Double.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW 2D Double/TCW 2D Double.lvclass"/>
				</Item>
				<Item Name="TCW Boolean" Type="Folder">
					<Item Name="TCW Boolean.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW Boolean/TCW Boolean.lvclass"/>
				</Item>
				<Item Name="TCW Double" Type="Folder">
					<Item Name="TCW Double.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW Double/TCW Double.lvclass"/>
				</Item>
				<Item Name="TCW I8" Type="Folder">
					<Item Name="TCW I8.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW I8/TCW I8.lvclass"/>
				</Item>
				<Item Name="TCW I16" Type="Folder">
					<Item Name="TCW I16.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW I16/TCW I16.lvclass"/>
				</Item>
				<Item Name="TCW I32" Type="Folder">
					<Item Name="TCW I32.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW I32/TCW I32.lvclass"/>
				</Item>
				<Item Name="TCW I64" Type="Folder">
					<Item Name="TCW I64.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW I64/TCW I64.lvclass"/>
				</Item>
				<Item Name="TCW Single" Type="Folder">
					<Item Name="TCW Single.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW Single/TCW Single.lvclass"/>
				</Item>
				<Item Name="TCW U8" Type="Folder">
					<Item Name="TCW U8.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW U8/TCW U8.lvclass"/>
				</Item>
				<Item Name="TCW U16" Type="Folder">
					<Item Name="TCW U16.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW U16/TCW U16.lvclass"/>
				</Item>
				<Item Name="TCW U32" Type="Folder">
					<Item Name="TCW U32.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW U32/TCW U32.lvclass"/>
				</Item>
				<Item Name="TCW U64" Type="Folder">
					<Item Name="TCW U64.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/Synchronous/TCW U64/TCW U64.lvclass"/>
				</Item>
			</Item>
			<Item Name="TwinCAT Writer.lvclass" Type="LVClass" URL="../TwinCAT Strict/Writer/TwinCAT Writer.lvclass"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="CheckWriteStatus.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Write/CheckWriteStatus.vi"/>
				<Item Name="Find Handle.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/_DependsOn/Utilities/Find Handle.vi"/>
				<Item Name="Init Reader.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Read/Init Reader.vi"/>
				<Item Name="Init Writer.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Write/Init Writer.vi"/>
				<Item Name="Internal Init.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/_DependsOn/Base/Init/Internal Init.vi"/>
				<Item Name="Internal Release.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/_DependsOn/Base/Release/Internal Release.vi"/>
				<Item Name="Notification Info.ctl" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/_DependsOn/TypeDefs/Notifications/Notification Info.ctl"/>
				<Item Name="Notification Mode.ctl" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/_DependsOn/TypeDefs/Notifications/Notification Mode.ctl"/>
				<Item Name="Reader Mode.ctl" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/_DependsOn/TypeDefs/Low-Levels/Reader Mode.ctl"/>
				<Item Name="Release Reader.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Read/Release Reader.vi"/>
				<Item Name="Release Writer.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Write/Release Writer.vi"/>
				<Item Name="Send Reader-Request.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Read/Send Reader-Request.vi"/>
				<Item Name="Send Writer-Request.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Write/Send Writer-Request.vi"/>
				<Item Name="TryReadData.vi" Type="VI" URL="/&lt;userlib&gt;/Beckhoff-LabVIEW-Interface/Low-Level/Read/TryReadData.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="TF3710Lib.dll" Type="Document" URL="TF3710Lib.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
